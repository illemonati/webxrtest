using UnityEngine;
using WebXR;

public class ColorCube : MonoBehaviour {
    public GameObject cube;
    public WebXRController controller;

    private Renderer cubeRenderer;

    private void Awake() {

        if (!controller) {
            controller = GetComponent<WebXRController>();
        }

        cubeRenderer = cube.GetComponent<Renderer>();
    }

    private void Update() {
        if (controller.GetButtonDown("Trigger")) {
            cubeRenderer.material.SetColor("_BaseColor", new Color(Random.value, Random.value, Random.value, cubeRenderer.material.color.a));
        }
        // workaround for button not resetting
        controller.GetButtonUp("Trigger");
    }
}