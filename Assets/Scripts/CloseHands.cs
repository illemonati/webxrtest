using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WebXR;


public class CloseHands : MonoBehaviour {
    private WebXRController controller;
    public Animator animator;


    private void Awake() {
        controller = GetComponent<WebXRController>();
        animator = GetComponent<Animator>();
    }

    private void Update() {
        var tGrip = controller.GetAxis("Grip");
        var tTrigger = controller.GetAxis("Trigger");
        var largestTime = Mathf.Max(tGrip, tTrigger);
        animator.Play("Take", -1, largestTime);
    }

}